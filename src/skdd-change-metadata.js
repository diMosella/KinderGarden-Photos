const fs      = require('fs');
const path = require('path');
const exiftool = require('node-exiftool');
const etp = new exiftool.ExiftoolProcess();

updateDates();

function updateDates() {
  let folder = path.resolve(`/Users/wim/Documents/foto-7`);

  fs.readdir(folder, updateFilesDate.bind({ folder }));
}

function updateFilesDate (err, fileNames) {
  if(err) {
    console.error(err.message);
  }
  console.log('folder', this.folder);
  const dateRegex = /^(\d{4})(\d{2})(\d{2})_/i;
  
  let etProcess = etp.open()
    .then((pid) => console.log('Started exiftool write process %s', pid));

  fileNames.forEach((name, index) => {
    
    const dateMatches = name.match(dateRegex);

    if (dateMatches) {
      const newMinute = (index % 60).toString().padStart(2, '0');
      const newSecond = Math.floor(Math.random() * 59).toString().padStart(2, '0');
      const newTimestamp = `${dateMatches[1]}:${dateMatches[2]}:${dateMatches[3]} 12:${newMinute}:${newSecond}+02:00`;

      const filePath = `${this.folder}/${name}`;
      console.log('Setting', filePath, newTimestamp);

      etProcess.then(() => etp.writeMetadata(filePath, {
        'Creator': 'SKDD',
        'DateTimeOriginal': newTimestamp,
        'FileCreateDate': newTimestamp,
        'FileModifyDate': newTimestamp
      }, ['overwrite_original']))
        .then(console.log, console.error);

      // etProcess.then(() => etp.readMetadata(filePath, ['Creator', 'DateTimeOriginal', 'FileCreateDate', 'FileModifyDate', 'FileAccessDate']))
      //     .then(console.log, console.error);
    }
  });


  etProcess.then(() => etp.close())
    .then(() => console.log('Closed exiftool for writing'), console.error);
}
